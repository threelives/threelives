import { Component, OnInit } from '@angular/core';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
    public tableData1: TableData;
    public tableData2: TableData;

    constructor() { }

    ngOnInit() {
        this.tableData1 = {
            headerRow: ['ID', 'Name', 'Email', 'View'],
            dataRows: [
                ['1', 'Alex Popescu', 'alex.popescu@gmail.com'],
                ['2', 'Marius Ionescu', 'marius.ionescu@gmail.com'],
                ['3', 'Bogdan Popovici', 'bogdan.popovici@gmail.com'],
                ['4', 'Mircea Popescu', 'mircea.popescu@gmail.com'],
                ['5', 'Iulian Ionescu', 'iulian.ionescu@gmail.com'],
            ]
        };
        this.tableData2 = {
            headerRow: ['ID', 'Name', 'Salary', 'Country', 'City'],
            dataRows: [
                ['1', 'Dakota Rice', '$36,738', 'Niger', 'Oud-Turnhout'],
                ['2', 'Minerva Hooper', '$23,789', 'Curaçao', 'Sinaai-Waas'],
                ['3', 'Sage Rodriguez', '$56,142', 'Netherlands', 'Baileux'],
                ['4', 'Philip Chaney', '$38,735', 'Korea, South', 'Overland Park'],
                ['5', 'Doris Greene', '$63,542', 'Malawi', 'Feldkirchen in Kärnten',],
                ['6', 'Mason Porter', '$78,615', 'Chile', 'Gloucester']
            ]
        };
    }

}
