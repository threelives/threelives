import { Component, OnInit } from '@angular/core';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { LegendItem, ChartType } from '../lbd/lbd-chart/lbd-chart.component';
import * as Chartist from 'chartist';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    public bloodChartType: ChartType;
    public bloodChartData: any;
    public bloodChartLegendItems: LegendItem[];
    public bloodChartOptions: any;

    constructor() { }

    ngOnInit() {

        this.bloodChartType = ChartType.Bar;

        this.bloodChartData = {
            labels: ['22% (A)', '17% (B)', '65% (AB)', '7% (0)'],
            series: [[22, 17, 65, 7]]
        };

        this.bloodChartLegendItems = [
            { title: 'A', imageClass: 'fa fa-circle text-info' },
            { title: 'B', imageClass: 'fa fa-circle text-danger' },
            { title: 'AB', imageClass: 'fa fa-circle text-warning' },
            { title: '0', imageClass: 'fa fa-circle text-primary' }
        ];

        this.bloodChartOptions = {
            high: 100,
            low: 0,
        };

    }
}
