<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 24/12/2018
 * Time: 22:23
 */

namespace ThreeLives;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use ThreeLives\Events\GrantPoints;
use ThreeLives\Listeners\GrantPointsListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        GrantPoints::class => [
            GrantPointsListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
