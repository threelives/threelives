<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 13:40
 */

namespace ThreeLives\Repositories;

interface BaseRepositoryInterface
{
    public function create(array $attributes);

    public function update(array $attributes, $id);

    public function all($columns = array('*'), $orderBy = 'id', $sortBy = 'desc');

    public function find($id);

    public function findOneOrFail($id);

    public function findBy(array $data);

    public function findOneBy(array $data);

    public function findOneByOrFail(array $data);

    public function paginateArrayResults(array $data, $perPage = 50);

    public function delete($id);
}
