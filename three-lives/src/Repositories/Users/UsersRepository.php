<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 13:43
 */

namespace ThreeLives\Repositories\Users;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use ThreeLives\Events\GrantPoints;
use ThreeLives\Models\Donation;
use ThreeLives\Models\UserDonnerCode;
use ThreeLives\Models\UserProfile;
use ThreeLives\Repositories\BaseRepository;

class UsersRepository extends BaseRepository implements UsersRepositoryInterface
{
    public function __construct(User $model)
    {
        $this->model = $model;

        //parent::__construct($model);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return array
     */
    public function validateAccount(User $user, Request $request)
    {
        $code = UserDonnerCode::where('code', $request->get('code'))->first();

        if(is_null($code)) {
            return [
                'success' => false,
                'message' => 'Invalid donner code.'
            ];
        }

        if(!is_null($code->user_id)) {
            return [
                'success' => false,
                'message' => 'Invalid donner code.'
            ];
        }

        $code->user_id = $user->id;
        $code->save();

        $user->code = $request->get('code');
        $user->save();

        return [
            'success' => true,
            'message' => 'Account validated.'
        ];
    }

    public function import(array $params)
    {
        foreach ($params as $key => $value)
        {
            $user = $this->model->where('donner_code', $value['donner_code'])->first();

            if(is_null($user)) {
                try {
                    $user = $this->model->create([
                        'email' => $value['email'],
                        'name' => $value['full_name'],
                        'password' => bcrypt('asdqwe123'),
                        'donner_code' => $value['donner_code']
                    ]);
                } catch (\Exception $exception) {

                }
            }

            $userDonnerCode = $this->__createUserDonnerCode([
                'donner_code' => $value['donner_code']
            ], $user);

            if(is_null($user->profile) && is_null($userDonnerCode->profile)) {
                $this->__createProfile([
                    'user_id' => is_null($user) ? null : $user->id,
                    'user_donner_code_id' => $userDonnerCode->id,
                    'blood_group' => $value['blood_group'],
                    'blood_rh' => $value['blood_rh']
                ]);
            }

            $this->__createDonation([
                'user_id' => is_null($user) ? null : $user->id,
                'user_donner_code_id' => $userDonnerCode->id,
                'donation_code' => $value['donation_code'],
                'donation_date' => isset($value['donation_date']) ? $value['donation_date'] : Carbon::today(),
                'weight' => $value['quantity']
            ], $user);
        }
    }

    protected function __createProfile(array $params)
    {
        return UserProfile::create($params);
    }

    protected function __createDonation(array $params, User $user)
    {
        try {
            $donation = Donation::create($params);

            event(new GrantPoints(compact('donation', 'user'), 'donation'));

            return $donation;
        } catch (\Exception $exception) {
        }
    }

    protected function __createUserDonnerCode(array $params, User $user = null)
    {
        $userDonnerCode = UserDonnerCode::where('code', $params['donner_code'])->first();

        if(is_null($userDonnerCode)) {
            $userDonnerCode = UserDonnerCode::create([
                'code' => $params['donner_code'],
                'user_id' => is_null($user) ? null : $user->id
            ]);
        }

        return $userDonnerCode;
    }
}
