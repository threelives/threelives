<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 13:43
 */

namespace ThreeLives\Repositories\Users;

use App\User;
use Illuminate\Http\Request;
use ThreeLives\Repositories\BaseRepositoryInterface;

interface UsersRepositoryInterface extends BaseRepositoryInterface
{
    public function import(array $params);

    /**
     * @param User $user
     * @param Request $request
     * @return array
     */
    public function validateAccount(User $user, Request $request);
}
