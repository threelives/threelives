<?php

Route::group([
    'prefix' => 'api/admin',
], function () {

    Route::middleware('jwt.auth')->get('users', function(Request $request) {
        return auth()->user();
    });

    Route::post('/login', 'Admin\Auth\LoginController@login');
    Route::post('/logout', 'Admin\Auth\LoginController@logout');


    Route::get('/users', 'Admin\UsersController@all');
    Route::post('/users', 'Admin\UsersController@create');
    Route::get('/users/{id}', 'Admin\UsersController@get');
    Route::put('/users/{id}', 'Admin\UsersController@update');
    Route::delete('/users/{id}', 'Admin\UsersController@delete');

    Route::get('/admins', 'Admin\AdminsController@all');
    Route::post('/admins', 'Admin\AdminsController@create');
    Route::get('/admins/{id}', 'Admin\AdminsController@get');
    Route::put('/admins/{id}', 'Admin\AdminsController@update');
    Route::delete('/admins/{id}', 'Admin\AdminsController@delete');

    Route::post('/import', 'Admin\ImportController@import');

    Route::get('/partners', 'Admin\PartnersController@all');
    Route::post('/partners', 'Admin\PartnersController@create');
    Route::get('/partners/{id}', 'Admin\PartnersController@get');
    Route::put('/partners/{id}', 'Admin\PartnersController@update');
    Route::delete('/partners/{id}', 'Admin\PartnersController@delete');
});
