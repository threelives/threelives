<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 12:22
 */

Route::group([
    'prefix' => 'api',
], function () {

    Route::any('register', 'Api\Auth\RegisterController@handle');
    Route::any('login', 'Api\Auth\LoginController@handle');

    Route::group([
        'middleware' => 'jwt.auth',
    ], function () {

        Route::post('/validate/account', 'Api\Auth\ValidateAccountController@handle');

        Route::post('/referral', 'Api\ReferralController@handle');

        Route::get('user/ping', 'Api\Auth\PingController@handle');

      Route::get('/me', 'Api\ProfileController@index');

      Route::group([
            'prefix' => 'profile',
        ], function () {
            Route::get('/', 'Api\Auth\ProfileController@index');

            Route::get('/history', 'Api\Auth\ProfileController@history');
        });
    });

});
