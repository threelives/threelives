<?php

namespace ThreeLives\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;


class BloodDonationsImport {

	public function import($file) {

		// get file data
		$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

		// read spreadsheet
		$spreadsheet = $reader->load($file);
		$worksheet = $spreadsheet->getActiveSheet();

		$data = [];
		$keys_map = [
			'A' => 'id',
			'B' => 'donner_code',
			'C' => 'donation_code',
			'D' => 'full_name',
			'E' => 'blood_group',
			'F' => 'blood_rh',
			'G' => 'quantity',
			'H' => 'pack_type',
			'I' => 'session_code',
			'J' => 'employee_name',
			'K' => 'description',
			'L' => 'phone',
			'M' => 'email'
		];

		// loop rows
		foreach($worksheet->getRowIterator() as $key=>$row) {

			// skip first row
			if($key > 1) {
				$cell_iterator = $row->getCellIterator();
				$row_data = [];

				// loop cells
				foreach($cell_iterator as $k=>$cell) {

					$val = (string) $cell->getValue();

					// remove parentheses from blood_group
					if($k == 'E') {
						$val = str_replace(['(', ')'], '', $val);
					}

					// remove punctuation from phone number
					if($k == 'L') {
						$val = str_replace(['(', ')', '.', '-', ' '], '', $val);
					}

					// add
					$row_data[$keys_map[$k]] = $val;

					// append sex from description
					if($k == 'K') {

						$val = explode('-', $val);

						$row_data['sex'] = null; // default
						if(isset($val[1]) && in_array($val[1], ['barbati', 'femei'])) {
							$row_data['sex'] = $val[1] == 'barbati' ? 'M' : 'femei';
						}
					}

					// if email wasn't provided, add it
					if(empty($row_data['email'])) {
						$row_data['email'] = null;
					}
				}

				$data[] = $row_data;
			}

		}

		// return formatted response
		return $data;


	}

}
