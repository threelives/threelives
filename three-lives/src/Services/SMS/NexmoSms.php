<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 07/12/2018
 * Time: 22:10
 */

namespace ThreeLives\Services\SMS;


use App\User;
use ThreeLives\Models\SmsLog;

class NexmoSms
{

    const API_KEY = '5ebfcd54';
    const API_SECRET = 'QZ3LvcRCPC6gXC8m';

    const NEXMO_FROM = '';

    public function __construct()
    {
    }

    public function send(array $params, User $user)
    {
        $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic(self::API_KEY, self::API_SECRET));

        $message = $client->message()->send([
            'to' => $params['to'],
            'from' => self::NEXMO_FROM,
            'text' => $params['message']
        ]);

        $log =  "Sent message to " . $message['to'] . ". Balance is now " . $message['remaining-balance'];

        SmsLog::create([
            'entity_id' => $user->id,
            'type' => 'user',
            'response' => $log,
            'request' => $log
        ]);
    }
}
