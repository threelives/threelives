<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 07/12/2018
 * Time: 22:10
 */

namespace ThreeLives\Services;


use ThreeLives\Services\SMS\NexmoSms;

class SmsService
{
    protected $nexmoSms;

    public function __construct(NexmoSms $nexmoSms)
    {
        $this->nexmoSms = $nexmoSms;
    }
}
