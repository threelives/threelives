<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 08/12/2018
 * Time: 19:09
 */

namespace ThreeLives\Controllers\Api;

use Illuminate\Http\Request;
use ThreeLives\Controllers\Controller;

class ReferralController extends Controller
{
    public function handle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'phone_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
    }
}
