<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 16:17
 */

namespace ThreeLives\Controllers\Api\Auth;

use ThreeLives\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use JWTFactory;
use JWTAuth;

class PingController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle()
    {
        try{
            $user = JWTAuth::parseToken()->authenticate();
        } catch (TokenExpiredException $e){
            return response()->json(['Page must be refreshed']);
        }
    }
}
