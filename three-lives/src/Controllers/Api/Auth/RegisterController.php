<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 12:26
 */

namespace ThreeLives\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;

use JWTFactory;
use JWTAuth;
use Validator;
use ThreeLives\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'phone_number' => 'required',
            'password'=> 'required'
        ]);

        if ($validator->fails()) {
            return response()
              ->json($validator->errors())
              ->setStatusCode(422);
        }
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'phone_number' => bcrypt($request->get('phone_number')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()
          ->json(compact('token'))
          ->setStatusCode(200);

    }
}
