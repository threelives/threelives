<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 27/12/2018
 * Time: 13:40
 */

namespace ThreeLives\Controllers\Api\Auth;


use Illuminate\Http\Request;
use ThreeLives\Controllers\Controller;

class ValidateAccountController extends Controller
{
    /**
     * Account validation
     *
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     *
     * @response {
     *  'success': true
     * }
     *
     * @return array
     */
    public function handle(Request $request)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);

        return $this->usersRepository->validateAccount(auth()->user(), $request);
    }
}
