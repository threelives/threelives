<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 27/12/2018
 * Time: 15:44
 */

namespace ThreeLives\Controllers\Api;

use ThreeLives\Controllers\Controller;

class ProfileController extends Controller
{
    public function index()
    {
        return ['user' => auth()->user()];
    }

    public function history()
    {

    }
}
