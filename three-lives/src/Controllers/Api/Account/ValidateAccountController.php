<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 07/12/2018
 * Time: 22:05
 */

namespace ThreeLives\Controllers\Api\Account;

use Illuminate\Http\Request;
use ThreeLives\Controllers\Controller;

class ValidateAccountController extends Controller
{
    public function handle(Request $request)
    {
        $user = auth()->user();

        $response = $this->usersRepository->validateAccount($user, $request);

        return $response;
    }
}
