<?php

namespace ThreeLives\Controllers;

use Illuminate\Http\Request;
use ThreeLives\Repositories\Users\UsersRepositoryInterface;
use ThreeLives\Services\BloodDonationsImport;

class ImportTest extends Controller
{
    //
   	protected $bloodDonationsImport;

   	protected $usersRepository;

    public function __construct(BloodDonationsImport $bloodDonationsImport, UsersRepositoryInterface $usersRepository)
    {
        $this->usersRepository = $usersRepository;

    	$this->bloodDonationsImport = $bloodDonationsImport;
    }

    public function import(Request $request) {

    	if($request->isMethod('post')) {

    		$file = $request->file('file');

	        $this->validate($request, [
	        	'import' => 'mimes:csv,xls,xlsx'
	        ]);

	        $response = $this->bloodDonationsImport->import($file->getRealPath());

	        $this->usersRepository->import($response);

    	}

    	// dd($this->bloodDonationsImport->import());

    	return view('Import/import');

    }
}
