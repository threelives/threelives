<?php
/**
 * Admins controller
 */

namespace ThreeLives\Controllers\Admin;

use Illuminate\Http\Request;

class PartnersController
{

    public function create(Request $request)
    {

    }

    public function all(Request $request)
    {

    }

    public function get($id)
    {

    }

    public function update(Request $request, $id)
    {

    }


    public function delete($id)
    {

    }
}
