<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 12:01
 */

namespace ThreeLives;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class ThreeLivesRouteServiceProvider extends ServiceProvider
{

    protected $namespace = 'ThreeLives\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('three-lives/src/routes/web.php'));

        Route::middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('three-lives/src/routes/api.php'));
    }


}
