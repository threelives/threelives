<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 07/12/2018
 * Time: 23:30
 */

namespace ThreeLives\Models;


use Illuminate\Database\Eloquent\Model;

class SmsLog extends Model
{
    protected $table = 'sms_logs';

    protected $fillable = [
        'entity_id',
        'type',
        'request',
        'response'
    ];
}
