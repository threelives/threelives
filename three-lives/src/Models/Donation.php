<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 14:10
 */

namespace ThreeLives\Models;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $table = 'donations';

    protected $fillable = [
        'user_id',
        'user_donner_code_id',
        'donation_code',
        'donation_date',
        'weight'
    ];
}
