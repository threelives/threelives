<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 07/12/2018
 * Time: 23:38
 */

namespace ThreeLives\Models;


use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $table = 'referrals';

    protected $fillable = [
        'user_id',
        'name',
        'email',
        'phone_number',
        'has_accepted',
        'code'
    ];
}
