<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 14:11
 */

namespace ThreeLives\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserDonnerCode extends Model
{
    protected $table = 'user_donner_code';

    protected $fillable = [
        'code',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function profile()
    {
        return $this->belongsTo(UserProfile::class, 'user_donner_code_id');
    }
}
