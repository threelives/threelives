<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 24/12/2018
 * Time: 21:56
 */

namespace ThreeLives\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserPointHistory extends Model
{
    protected $table = 'user_points_history';

    protected $fillable = [
        'user_id',
        'description',
        'points',
        'type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
