<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 14:13
 */

namespace ThreeLives\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profile';

    protected $fillable = [
        'user_id',
        'user_donner_code_id',
        'blood_group',
        'blood_rh'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function donner()
    {
        return $this->belongsTo(UserDonnerCode::class, 'user_donner_code_id');
    }
}
