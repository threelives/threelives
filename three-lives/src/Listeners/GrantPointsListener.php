<?php

namespace ThreeLives\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use ThreeLives\Events\GrantPoints;
use ThreeLives\Models\UserPointHistory;

class GrantPointsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(array $params)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GrantPoints $event
     * @return void
     */
    public function handle(GrantPoints $event)
    {
        /**
         * @TODO:
         *  1/ Grant user points
         *  2/ Notify the user if needed
         */

        if ($event->type == 'donation') {
            UserPointHistory::create([
                'user_id' => $event->params['user']->id,
                'description' => 'Blood donation: ' . $event->params['donation']->donation_code,
                'points' => 100,
                'type' => 'win'
            ]);
        }

    }
}
