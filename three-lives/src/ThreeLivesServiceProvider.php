<?php
/**
 * Created by PhpStorm.
 * User: mihaisolomon
 * Date: 17/11/2018
 * Time: 10:39
 */

namespace ThreeLives;

use Illuminate\Support\ServiceProvider;
use ThreeLives\Repositories\Users\UsersRepository;
use ThreeLives\Repositories\Users\UsersRepositoryInterface;

class ThreeLivesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->app->bind(
            UsersRepositoryInterface::class,
            UsersRepository::class
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
