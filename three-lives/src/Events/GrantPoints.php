<?php

namespace ThreeLives\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GrantPoints
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $params;

    public $type;

    /**
     * Create a new event instance.
     *
     * @param array $params
     * @param string $type
     *
     * @return void
     */
    public function __construct(array $params, $type)
    {
        $this->params = $params;

        $this->type = $type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
